/*
Write a recursive function fibonacciSeq(n) to print the fibonacci sequence up to n.

For example fibonacciSeq(10) should print the following numbers on the terminal.

0

1

1

2

3

5

8

13

21

34

55
*/

#include<stdio.h>

int main(){

    int n,range;

    printf("\nEnter Range : ");
    scanf("%d", &range);

    for(n = 0; n < range; n++)
    {
        printf("\n %d \n", fibonacciSeq(n));
    }

    return 0;

}

int fibonacciSeq(int number){

    if(number ==0 || number ==1)
    {
        return number;
    }
    else
    {
        return fibonacciSeq(number-1) + fibonacciSeq(number-2);
    }

}
